var express = require('express');
var router = express.Router();
var func = require('../models/functions');

router.get('/', function(req, res, next)
{
    res.send("Welcome to PaymentApp!");
});

router.get("/payment/token" , function(req , res)
{
    func.getToken(req , res);
});

router.post("/payment/verify" , function(req , res)
{
    func.commit(req , res);
});

router.get("/payment/check" , function(req , res)
{
    func.check(req , res);
});

module.exports = router;
