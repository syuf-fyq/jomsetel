async function getToken(req , res)
{
	var chars = "abcdefghijklmnopqrstuvwxyz0123456789";
	var token = "";

	while (token.length < 20)
	{
		var rand = Math.floor(Math.random() * chars.length);
		var char = chars.substring(rand , (rand + 1));

		token += char;
	}

	res.json({"success" : true , "message" : "Successful generated token." , "token" : token});

	return true;
}

async function commit(req , res)
{
	if(!req.body.token)
		return res.json({"success" : false , "message" : "Invalid token."});

	res.json({"success" : true , "message" : "Commit successful." , "result" : "PENDING"});

	return true;
}

async function check(req , res)
{
	if(!req.query.token)
		return res.json({"success" : false , "message" : "Invalid token."});

	var status = ["CONFIRMED" , "DECLINED"];

	var rand   = Math.floor(Math.random() * status.length);

	res.json({"success" : true , "message" : "Check successful." , "result" : status[rand]});

	return true;
}

module.exports = {
	getToken ,
	commit ,
	check
}