# README #


### Pre-Requisite ###

* Docker
* Docker Compose

### Getting Started ###

* Ensure that you have instaled Docker and Docker Compose.
* Store all four (4) directories in a single directory of your choice. (e.g.: /opt/jomsetel)
* Go to each directory (except "compose") and run `npm install`.
* Run `docker-compose -f jomsetel.yaml up -d`
* Access the DB docker container and execute SQL scripts in `ordersapp/db` directory.
* You might want to update `ordersapp/.env` and `jomsetelui/.env`.