var express = require('express');
var router = express.Router();
var func = require("../models/functions");

router.get("/", function(req, res, next)
{
    res.send("Welcome to OrdersApp!");
});

router.post("/order/create", function (req, res)
{
    func.orderCreate(req , res);
});

router.post("/order/cancel" , function(req , res)
{
    func.orderCancel(req , res);
});

router.get("/order/check" , function(req , res)
{
    func.orderCheck(req , res);
});

router.post("/order/deliver" , function(req , res)
{
    func.orderDeliver(req , res);
});

router.post("/order/pay" , function(req , res)
{
    func.orderPay(req , res);
});

router.get("/order/list" , function(req , res) {
    func.orderList(req , res);
})

router.get("/product/list" , function(req , res)
{
    func.productList(req , res);
});

module.exports = router;
