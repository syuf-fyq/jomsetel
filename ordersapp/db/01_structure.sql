CREATE TABLE `user`
(
	id			 INT		  NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    username 	 VARCHAR(50)  NOT NULL ,
    `password` 	 VARCHAR(100) NOT NULL ,
    date_created DATETIME	  NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE `order`
(
	id			 INT	  NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    user_id		 INT	  NOT NULL REFERENCES `user` (id) ,
    `status`	 INT  	  NOT NULL ,
    date_created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE product
(
	id		  	 INT		   NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    `name`	 	 VARCHAR(100)  NOT NULL ,
    quantity 	 INT		   NOT NULL DEFAULT 0 ,
    price	 	 DOUBLE(9 , 2) NOT NULL ,
    discount 	 INT 		   NOT NULL DEFAULT 0 ,
    date_created DATETIME 	   NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE item
(
	id		   INT			 NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    order_id   INT			 NOT NULL REFERENCES `order` (id) ,
    product_id INT			 NOT NULL REFERENCES `product` (id) ,
    quantity   INT			 NOT NULL DEFAULT 1 ,
    price	   DOUBLE(9 , 2) NOT NULL ,
    discount   INT 		   	 NOT NULL DEFAULT 0
);