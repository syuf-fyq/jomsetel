const Order = require("../models/order");

async function orderCreate(req , res)
{
	if(!req.body.products || !Array.isArray(req.body.products))
		return res.json({"success" : false , "message" : "Invalid product list."});

	var Order = require("./order");

    var order    = new Order(1);
    var products = req.body.products;

    products.forEach((product) => {
        order.addProduct(product);
    });

	var result = await order.store();

	if(result) res.json({"success" : true  , "message" : "Successfully created order : " + order.id , "id" : order.id});
	else       res.json({"success" : false , "message" : "Unable to create order."});
}

async function orderCancel(req , res)
{
	if(!req.body.id)
		return res.json({"success" : false , "message" : "Missing/Invalid Order ID"});

	var orderId = req.body.id;

	var Order = require("./order");

	var order  = new Order(1);
	var result = await order.cancel(orderId);

	if(result) res.json({"success" : true  , "message" : "Successfully canceled order : " + orderId});
	else       res.json({"success" : false , "message" : "Unable to cancel order."});
}

async function orderCheck(req , res)
{
	if(!req.query.token)
		return res.json({"success" : false , "message" : "Missing/Invalid Token"});

	if(!req.query.id)
		return res.json({"success" : false , "message" : "Missing/Invalid Order Id"});
	
	var Order = require("./order");
	var order = new Order(1);

	var orderId = req.query.id;
	var token   = req.query.token;
	var result  = await order.check(token);

	if(result === "DECLINED") 		 order.cancel(orderId);
	else if (result === "CONFIRMED") order.confirm(orderId);

	if(result !== false) res.json({"success" : true  , "message" : "Successfully retrieved order payment status : " + token , "result" : result});
	else       			 res.json({"success" : false , "message" : "Unable to retrieve order."});
}

async function orderDeliver(req , res)
{
	if(!req.body.id)
		return res.json({"success" : false , "message" : "Missing/Invalid Order ID"});

	var orderId = req.body.id;

	var Order = require("./order");

	var order  = new Order(1);

	var result = await order.deliver(orderId);

	if(result !== false) res.json({"success" : true  , "message" : "Successfully set order as delivered : " + orderId});
	else       			 res.json({"success" : false , "message" : "Unable to set order as delivered."});
}

async function orderPay(req , res)
{
	if(!req.body.id)
		return res.json({"success" : false , "message" : "Missing/Invalid Order ID"});

	var orderId = req.body.id;
	var Order 	= require("./order");
	var order 	= new Order(1);

	var result = await order.pay(orderId);

	if(result !== false) res.json({"success" : true  , "message" : "Successfully performed payment." , "result" : result.result , "token" : result.token});
	else       			 res.json({"success" : false , "message" : "Unable to perform payment."});
}

async function orderList(req , res)
{
	var Order 	= require("./order");
	var order 	= new Order(1);

	var result = await order.list();

	if(result && result.length > 0)
	{
		for(var i in result)
		{
			result[i]["statusName"] = order.getStatus(result[i].status);
		}
	}

	if(result !== false) res.json({"success" : true  , "message" : "Successfully retrieved order list." , "result" : result});
	else       			 res.json({"success" : false , "message" : "Unable to retrieve order list."});
}

async function productList(req , res)
{
	var Product = require("./product");

	var product = new Product();
	var result  = await product.getAll();

	if(result !== false) res.json({"success" : true  , "message" : "Successfully retrieved order list." , "result" : result});
	else       			 res.json({"success" : false , "message" : "Unable to retrieve order."});
}

module.exports = {
	orderCreate ,
	orderCancel ,
	orderCheck ,
	orderDeliver ,
	orderPay ,
	orderList ,
	productList
}