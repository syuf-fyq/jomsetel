class Product
{
	async getAll()
	{
		var Mysql = require("./mysql");
		var mysql = new Mysql();

		var result = await mysql.execute("SELECT * FROM product ORDER BY date_created DESC");

		if(result.length < 1)
			return null;

		return result;
	}

	async getById(id)
	{
		var Mysql = require("./mysql");
		var mysql = new Mysql();

		var result = await mysql.execute("SELECT * FROM product WHERE id = '" + id + "'");

		if(result.length < 1)
			return null;

		return result[0];
	}
}

module.exports = Product;