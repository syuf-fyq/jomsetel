class Mysql
{
	constructor()
	{
		this.mysql = require("mysql");

		this.host	  = process.env.DB_HOST;
		this.username = process.env.DB_USER;
		this.password = process.env.DB_PASS;
		this.database = process.env.DB_NAME;

		this.conn = this.mysql.createConnection({"host" : this.host , "user" : this.username , "password" : this.password , "database" : this.database});
	}

	async execute(sql)
	{
		try 
		{
			return new Promise((resolve , reject) => 
			{
				this.conn.query(sql , (err , rows) =>
				{
					if(err) return reject(err);

					resolve(rows);
				});
			});
		}
		catch(error)
		{
			console.log(error);
			return [];
		}
	}

	escape(value)
	{
		return this.mysql.escape(value);
	}

	close()
	{
		this.conn.end();
	}
}

module.exports = Mysql;