const Product = require("./product");

const CREATED 	= 1;
const CONFIRMED = 2;
const DELIVERED = 3;
const CANCELLED = 4;

class Order
{
	constructor(userId)
	{
		this.products = [];
		this.userId   = userId;
	}

	addProduct(product)
	{
		this.products.push(product);
	}

	async getById(id)
	{
		var Mysql = require("./mysql");
		var mysql = new Mysql();

		var result = await mysql.execute("SELECT * FROM `order` WHERE id = " + mysql.escape(id));

		mysql.close();

		if(result.length < 1)
			return null;

		return result[0];
	}

	async getItemsByOrder(id)
	{
		var Mysql = require("./mysql");

		var mysql = new Mysql();

		var result = await mysql.execute("SELECT * FROM item WHERE order_id = '" + mysql.escape(id) + "'");

		mysql.close();

		if(result.length < 1)
			return [];

		for(var i in result)
		{
			var item = result[i];

			item["price_subtotal"] = (100 - item.discount) / 100  * item.price * item.quantity;

			result[i] = item;
		}

		return result;
	}

	getStatus(status)
	{
		switch(status)
		{
			case CREATED   : return "Created";
			case CONFIRMED : return "Confirmed";
			case DELIVERED : return "Delivered";
			case CANCELLED : return "Cancelled";
		}
	}

	async pay(id)
	{
		try
		{
			var result = await this.getById(id);
			
			if(!result || result == null)
				return false;

			result = await this.getItemsByOrder(id);
			
			if(!result || result == null)
				return false;
				
			var total = 0;

			result.forEach(item => {
				total += item.price_subtotal
			});

			const axios = require('axios');

			result = await axios.get(process.env.PAYMENTURL + "/payment/token");

			if(!result.data || !result.data.success)
				return false;

			var token = result.data.token;

			result = await axios.post(process.env.PAYMENTURL + "/payment/verify" ,
			{
				"token" : token ,
				"total" : total
			});

			if(!result.data || !result.data.success)
				return false;

			return {"result" : result.data.result , "token" : token};
		}
		catch(err)
		{
			console.log(err);

			return false;
		}
	}

	summarizeItem(items)
	{
		if(items.length < 1)
			return {"total" : 0};

		var total = 0;

		for(var i in items)
		{
			var item = items[i];

			total += item["price_subtotal"];
		}

		return {"total" : parseFloat(total)};
	}

	async cancel(id)
	{
		var Mysql = require("./mysql");

		var orderDetail = await this.getById(id);

		if(orderDetail == null)
			return false;

		var mysql = new Mysql();

		try
		{
			await mysql.execute("UPDATE `order` " +
								"   SET `status` = '" + CANCELLED + "' " +
								" WHERE id = " + mysql.escape(id));

			return true;
		}
		catch(error)
		{
			console.log(error);

			return false;
		}
		finally
		{
			mysql.close();
		}
	}

	async deliver(id)
	{
		var Mysql = require("./mysql");

		var orderDetail = await this.getById(id);

		if(orderDetail == null)
			return false;

		var mysql = new Mysql();

		try
		{
			await mysql.execute("UPDATE `order` " +
								"   SET `status` = '" + DELIVERED + "' " +
								" WHERE id = " + mysql.escape(id));

			return true;
		}
		catch(error)
		{
			console.log(error);

			return false;
		}
		finally
		{
			mysql.close();
		}
	}

	async confirm(id)
	{
		var Mysql = require("./mysql");

		var orderDetail = await this.getById(id);

		if(orderDetail == null)
			return false;

		var mysql = new Mysql();

		try
		{
			await mysql.execute("UPDATE `order` " +
								"   SET `status` = '" + CONFIRMED + "' " +
								" WHERE id = " + mysql.escape(id));

			return true;
		}
		catch(error)
		{
			console.log(error);

			return false;
		}
		finally
		{
			mysql.close();
		}
	}

	async check(token)
	{
		try
		{
			const axios = require('axios');

			var result = await axios.get(process.env.PAYMENTURL + "/payment/check?token=" + token);

			if(!result.data || !result.data.success)
				return false;

			var status = result.data.result;

			return status;
		}
		catch(error)
		{
			console.log(error);
			return false
		}
	}

	async list()
	{
		try
		{
			var Mysql = require("./mysql");

			var mysql = new Mysql();

			var result = await mysql.execute("SELECT o.* , i.total " 		+
											 "  FROM `order` AS o " 			+
											 "  JOIN ( " 					+
											 "			SELECT order_id "	+
											 "			 , SUM(((100 - discount) / 100 * price) * quantity) AS total " +
											 "			  FROM `item` " 		+
											 "			 GROUP BY order_id " 	+
											 "  ) AS i ON i.order_id = o.id " 	+
											 " WHERE o.user_id = '" + mysql.escape(this.userId) + "' " +
											 " ORDER BY o.date_created DESC");

			if(result.length < 1)
				return null;
	
			return result;
		}
		catch(error)
		{
			console.log(error);

			return null;
		}
	}

	async store()
	{
		var Product = require("./product");
		var Mysql   = require("./mysql");
		var mysql 	= new Mysql();

		try
		{
			var result;

			result = await mysql.execute("INSERT INTO `order` (user_id , `status`) " + 
										 "	   VALUES ('" + mysql.escape(this.userId) + "' , '" + CREATED + "')");

			this.id = result.insertId;

			var prod = new Product();

			for(var i in this.products)
			{
				var product 	  = this.products[i];
				var productDetail = await prod.getById(product.id);
				
				if(productDetail == null)
					break;

				var price 	 = productDetail.price;
				var discount = productDetail.discount;

				mysql.execute("INSERT INTO item (order_id , product_id , quantity , price , discount) " +
							  "			 VALUES ('" + mysql.escape(this.id) + "' , '" + mysql.escape(product.id) + "' , '" + mysql.escape(product.quantity) + "' , '" + mysql.escape(price) + "' , '" + mysql.escape(discount) + "')");
			}

			return true;
		}
		catch(error)
		{
			console.log(error);

			return false;
		}
		finally
		{
			mysql.close();
		}
	}
}

module.exports = Order;