import React from 'react';
import Cart from './Cart';
import env from 'react-dotenv';

class Product extends React.Component
{
	constructor() {
		super();

		this.state =
		{
			"productList" 	 : [] ,
			"currentSection" : "products"
		};

		this.toProduct = this.toProduct.bind(this);
	}

	componentDidMount()
	{
		this.getProduct();
	}
	
	async getProduct()
	{
		const axios = require('axios');

		var result = await axios.get(env.APIURL + "/product/list");

		if(!result.data || !result.data.success)
			return;

		var productList = result.data.result;

		for(var i in productList)
		{
			productList[i]["priceAfterDiscount"] = (100 - productList[i].discount) / 100 * productList[i].price;
			productList[i]["selectedQuantity"]	 = 0;
		}

		this.setState({"productList" : productList});
	}

	toProduct = () => {
		this.setState({"currentSection" : "products"});
	}

	toggleQuantity(productId , quantity)
	{
		var productList = this.state.productList;

		for(var i in productList)
		{
			var id = productList[i].id;

			if(productId === id)
			{
				productList[i].selectedQuantity += quantity;				
			}
		}

		this.setState({"productList" : productList});
	}

	productGrid()
	{
		return (
			<div className="product-list">
				<div className="row sect-header">
					<div className="col-10">
						<h3 className="sect-head text-info">Products</h3>
					</div>
					<div className="col-2 text-right">
						<button className="btn btn-outline-info" type="button" onClick={() => {this.setState({"currentSection" : "cart"})}}>
							<i className="bi bi-cart3"></i> Cart
						</button>
					</div>
				</div>
				<div className="p-3">
					<div className="row">
						{
							this.state.productList.map((product) =>
							(
								<div className="col-md-4 mb-3">
									<div className="card pointer">
										<div className="card-body" onClick={()=>{ this.toggleQuantity(product.id , 1) }}>
											<strong>{product.name}</strong>
											<br/>
											RM {product.priceAfterDiscount.toFixed(2)} {product.price !== product.priceAfterDiscount ? (
												<span className="text-stroke">{product.price.toFixed(2)}</span>
											) : ""}
										</div>
										<div className="card-footer card-product-selected bg-success" style={{
											"visibility" : (product.selectedQuantity < 1 ? 'hidden' : '')
										}}>
											Quantity : <button className="btn btn-sm btn-light py-0" onClick={()=>{ this.toggleQuantity(product.id , -1) }}>-</button>
											<span className="mx-2">{product.selectedQuantity}</span>
											<button className="btn btn-sm btn-light py-0"  onClick={()=>{ this.toggleQuantity(product.id , 1) }}>+</button>
										</div>
									</div>
								</div>
							))
						}
					</div>
				</div>
				<div>
					<div id="modal-new-order" className="modal fade" tabIndex="-1">
						<div className="modal-dialog modal-xl">
							<div className="modal-content">
								<div className="modal-header">
									<h5 className="modal-title">New Order</h5>
									<button type="button" className="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div className="modal-body">
									<div className="form-group row">
										<div className="col-8">
											<select className="form-control" placeholder="Select a product...">
												<option></option>
												<option>Chair</option>
												<option>Table</option>
											</select>
										</div>
										<div className="col-3">
											<select className="form-control" placeholder="Select a quantity...">
												<option>0</option>
												<option>1</option>
												<option>2</option>
											</select>
											<span className="text-right" style={{fontSize:"0.75rem" , display:"block"}}> (Max: 100)</span>
										</div>
										<div className="col-1">
											<button type="button" className="btn btn-outline-danger"><i className="bi bi-x-circle-fill"></i></button>
										</div>
									</div>
								</div>
								<div className="modal-footer">
									<button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="button" className="btn btn-primary">Next &nbsp; <i className="bi bi-skip-forward-fill"></i></button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}

	render()
	{
		return (
			<div>
			{
				(this.state.currentSection === "products" ? this.productGrid() : (<Cart Product={this} toProduct={this.toProduct} />))
			}
			</div>
		);
	}
}

export default Product;