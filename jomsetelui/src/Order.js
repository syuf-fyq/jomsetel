import React from 'react';
import env from 'react-dotenv';

class Order extends React.Component
{
	constructor(prop)
	{
		super(prop);

		this.state = {
			"orderList" : []
		}
	}

	componentDidMount()
	{
		this.getOrders();
	}

	async getOrders()
	{
		const axios = require('axios');

		var result = await axios.get(env.APIURL + "/order/list");

		if(!result.data || !result.data.success)
			return false;

		this.setState({"orderList" : result.data.result});
	}

	async deliverOrder(id)
	{
		const axios = require('axios');

		var result = await axios.post(env.APIURL + "/order/deliver" , {
			"id" : id
		});

		if(!result.data || !result.data.success)
			return false;

		this.getOrders();
	}

	async cancelOrder(id)
	{
		const axios = require('axios');

		var result = await axios.post(env.APIURL + "/order/cancel" , {
			"id" : id
		});

		if(!result.data || !result.data.success)
			return false;

		this.getOrders();
	}

	render()
	{
		return (
			<div className="cart-list">
				<div className="row sect-header">
					<div className="col-12">
						<h3 className="sect-head text-info">Your Orders</h3>
					</div>
				</div>
				<table className="table cart-list-table">
					<thead className="thead-dark">
						<tr>
							<th scope="col">Order No.</th>
							<th scope="col">Date</th>
							<th scope="col">Status</th>
							<th scope="col">Total (RM)</th>
							<th scope="col">Action</th>
						</tr>
					</thead>
					<tbody>
						{
							this.state.orderList.map((order) => (
								<tr>
									<td>{order.id}</td>
									<td>{order.date_created}</td>
									<td style={{fontWeight : "bold" , color:(
										order.status === 2 || order.status === 3 ? "green" : (
											order.status === 4 ? "red" : "black"
										)
									)}}>{order.statusName}</td>
									<td>{order.total.toFixed(2)}</td>
									<td>
										{
											order.status === 2 ? (
												<button className="btn btn-sm btn-success" alt="Delivered" onClick={()=>{this.deliverOrder(order.id)}}>
													<i className="bi bi-truck"></i>
												</button>
											) : "" 
										}
										&nbsp;
										{
											order.status === 1 || order.status === 2 ? (
												<button className="btn btn-sm btn-danger" alt="Cancel" onClick={()=>{this.cancelOrder(order.id)}}>
													<i className="bi bi-x-circle-fill"></i>
												</button>
											) : "" 
										}
									</td>
								</tr>
							))
						}
						
					</tbody>
				</table>
			</div>
		);
	}
}

export default Order;