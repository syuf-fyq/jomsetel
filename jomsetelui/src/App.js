import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import './App.css';
import Product from './Product';
import Order from './Order';

class App extends React.Component {

    render() {
        return (
            <Router>
                <div>
                    <div className="top-nav">
                        <h1>JomSetel</h1>
                        <div className="float-right right-text mr-3"><a href="./#">Login</a> | <a href="./#">Register</a></div>
                    </div>
                    <div className="sec-nav">
                        <div className="container">
                            <ul className="nav justify-content-center">
                                <li className="nav-item">
                                    <Link className="nav-link" to="/">Home</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="/products">Products</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="/orders">Orders</Link>
                                </li>
                            </ul>
                        </div>
                    </div>              
                    <div className="container">
                        <Switch>
                            <Route exact path="/">
                                <Product />
                            </Route>
                            <Route exact path="/products">
                                <Product />
                            </Route>
                            <Route exact path="/orders">
                                <Order />
                            </Route>
                        </Switch>
                    </div>
                    <div className="footer-border"></div>
                    <div className="container-fluid footer">
                        Copyright (c) 2021 JomSetel
                    </div>
                </div>
            </Router>
        );
    }
}

export default App;
