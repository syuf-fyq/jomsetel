import React from 'react';
import env from 'react-dotenv';

class Cart extends React.Component
{
	constructor(prop)
	{
		super(prop)

		this.state = {
			"totalCart" 			: 0 ,
			"paymentStatus" 		: "" ,
			"orderId"				: "" ,
			"disablePayNowButton" 	: false ,
			"token"					: ""
		}

		this.prop 	 = prop;
		this.Product = prop.Product;
	}

	calculateTotal()
	{
		var total = 0;

		this.Product.state.productList.forEach((product) => {
			if(product.selectedQuantity > 0) {
				total += (product.priceAfterDiscount * product.selectedQuantity);
			}
		});

		this.setState({"totalCart" : total});
	}

	async payNow()
	{
		const axios = require('axios');

		this.setState({"disablePayNowButton" : true});

		var productList = [];

		this.Product.state.productList.forEach((product) => {
			
			if(product.selectedQuantity > 0)
			{
				productList.push({
					"id" 		: product.id ,
					"quantity" 	: product.selectedQuantity ,
					"price" 	: product.price ,
					"discount" 	: product.discount
				});
			}
		});

		var result = await axios.post(env.APIURL + "/order/create" , {
			"products" : productList
		});

		if(!result.data || !result.data.success)
			return;

		var orderId = result.data.id;

		this.setState({"orderId" : orderId});

		result = await axios.post(env.APIURL + "/order/pay" , {
			"id" : orderId
		});

		if(result.data.result)
		{
			this.setState({"paymentStatus" : result.data.result , "token" : result.data.token});

			if(result.data.result === "PENDING")
			{
				setTimeout(() => this.checkStatus() , 5000);
			}
		}
	}

	async checkStatus()
	{
		const axios = require('axios');

		var result = await axios.get(env.APIURL + "/order/check?token=" + this.state.token + "&id=" + this.state.orderId);

		if(!result.data || !result.data.success)
		{
			setTimeout(() => this.checkStatus() , 5000);
			return;
		}
		
		this.setState({"paymentStatus" : result.data.result , "disablePayNowButton" : false});
	}

	componentDidMount()
	{
		this.calculateTotal();
	}

	render()
	{
		return (
			<div className="cart-list">
				<div className="row sect-header">
					<div className="col-9">
						<button className="btn btn-outline-info mr-3 btn-sm" onClick={()=>{this.prop.toProduct()}}>
							<i className="bi bi-backspace-fill"></i>
						</button>
						<h3 className="sect-head text-info" style={{display:"inline-block"}}>Your Cart</h3>
					</div>
					<div className="col-3 text-right">
						{this.state.paymentStatus === "" ? "" : (
							<strong>Order #{this.state.orderId} <br />{this.state.paymentStatus}</strong>
						)}
						
					</div>
				</div>
				<table className="table cart-list-table">
					<thead className="thead-dark">
						<tr>
							<th scope="col">Product No.</th>
							<th scope="col">Date</th>
							<th scope="col">Item</th>
							<th scope="col">Quantity</th>
							<th scope="col">Total (RM)</th>
						</tr>
					</thead>
					<tbody>
						{
							this.Product.state.productList.map((product) => (
								product.selectedQuantity > 0 ? (

								<tr>
									<td>{product.id}</td>
									<td>{product.date_created}</td>
									<td>{product.name}</td>
									<td>{product.selectedQuantity}</td>
									<td>{(product.priceAfterDiscount * product.selectedQuantity).toFixed(2)}</td>
								</tr>

								) : ""
							))
						}
						
					</tbody>
				</table>
				<div className="p-3 text-right summary">
					<strong>Total : <span className="text-danger">RM {this.state.totalCart.toFixed(2)}</span></strong>
					{
						this.state.paymentStatus === "" || this.state.paymentStatus === "PENDING" ? (
							<button type="button" className="btn btn-success btn-pay" disabled={this.state.disablePayNowButton} onClick={() => this.payNow()}>
								<i className="bi bi-wallet2"></i> Pay Now
							</button>
						) : ""
					}
					
				</div>
			</div>
		);
	}
}

export default Cart;